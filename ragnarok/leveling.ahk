﻿LevelHeroAt(XLoc, YLoc, UseMulti) {
	global
	if UseMulti
		ControlSend,, {z down}, %GameName%
    sleep 60
	ControlClick, x%XLoc% y%YLoc%, %GameName%,,,, NA
	if UseMulti
		ControlSend,, {z up}, %GameName%
    sleep 60
    return
}

LevelSkillsAt(XLoc, YLoc) {
	global
	local SkillLoc1 := (XLoc + 95)
	local SkillLoc2 := (XLoc + 125)
	local SkillLoc3 := (XLoc + 170)
	local SkillLoc4 := (XLoc + 200)
	local SkillLoc5 := (XLoc + 240)
	local SkillLoc6 := (XLoc + 275)
	local SkillLoc7 := (XLoc + 310)

	ControlClick, x%SkillLoc1% y%YLoc%, %GameName%,,,, NA
	ExitTranscendMenu()
	sleep 60
	ControlClick, x%SkillLoc2% y%YLoc%, %GameName%,,,, NA
	ExitTranscendMenu()
	sleep 60
	ControlClick, x%SkillLoc3% y%YLoc%, %GameName%,,,, NA
	ExitTranscendMenu()
	sleep 60
	ControlClick, x%SkillLoc4% y%YLoc%, %GameName%,,,, NA
	ExitTranscendMenu()
	sleep 60
	ControlClick, x%SkillLoc5% y%YLoc%, %GameName%,,,, NA
	ExitTranscendMenu()
	sleep 60
	ControlClick, x%SkillLoc6% y%YLoc%, %GameName%,,,, NA
	ExitTranscendMenu()
	sleep 60
	ControlClick, x%SkillLoc7% y%YLoc%, %GameName%,,,, NA
	ExitTranscendMenu()
	return
}

LevelCurrentHeroPage(Outlier) {
	global
	LevelSkillsAt(TopChar1X, TopChar1Y + Outlier)
	LevelSkillsAt(TopChar2X, TopChar2Y + Outlier)
	LevelSkillsAt(TopChar3X, TopChar3Y + Outlier)

	LevelHeroAt(TopChar1X, TopChar1Y + Outlier, false)
	LevelHeroAt(TopChar2X, TopChar2Y + Outlier, false)
	LevelHeroAt(TopChar3X, TopChar3Y + Outlier, false)
	return
}

LevelTopHeroes() {
	global
	ScrollToTop()
	sleep 50
	LevelCurrentHeroPage(0)
	LevelCurrentHeroPage(10)
	LevelCurrentHeroPage(15)
	return
}

LevelBotHeroes(Outlier) {
	global
	ScrollToBot()
	sleep 60
	LevelSkillsAt(BotChar3X, BotChar3Y + Outlier)
	LevelSkillsAt(BotChar2X, BotChar2Y + Outlier)
	LevelSkillsAt(BotChar1X, BotChar1Y + Outlier)

	LevelHeroAt(BotChar3X, BotChar3Y + Outlier, true)
	LevelHeroAt(BotChar2X, BotChar2Y + Outlier, true)
	LevelHeroAt(BotChar1X, BotChar1Y + Outlier, true)
	return
}

LevelAllSkills() {
	global
	ScrollTobot()
	Loop 3
	{
		ControlClick, x%AllSkillsX% y%AllSkillsY%, %GameName%,,,, NA
		sleep 60
	}
	return
}

LevelHeroLoop() {
	global
	LevelAllSkills()
	LevelTopHeroes()
	Loop 20
	{
		ScrollDown()
		LevelCurrentHeroPage(0)
		LevelCurrentHeroPage(10)
		LevelCurrentHeroPage(15)
		sleep 60
	}
	LevelBotHeroes(0)
	LevelAllSkills()
	return
}

ExitTranscendMenu() {
	global
	ControlClick, x%ExitX% y%ExitY%, %GameName%,,,, NA
	return
}