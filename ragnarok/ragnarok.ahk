#InstallKeybdHook
#UseHook
#Persistent
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#SingleInstance force
SetControlDelay -1

GameName = Ragnarok Clicker
Paused = false

;; Main
MainX = 850
MainY = 380

;; Scrolling
ScrollX = 554
ScrollTopY = 236
ScrollBotY = 628
ScrollUpY = 222
ScrollDownY = 653

;; Character Updates
TopChar1X = 110
TopChar1Y = 285

TopChar2X = 110
TopChar2Y = 390

TopChar3X = 110
TopChar3Y = 495

TopChar4X = 110
TopChar4Y = 600

BotChar1X = 100
BotChar1Y = 300

BotChar2X = 100
BotChar2Y = 431

BotChar3X = 100
BotChar3Y = 541

AllSkillsX = 386
AllSkillsY = 601

ExitX = 906
ExitY = 162

WinGet, GameID, ID, WinTitle %GameName%

SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
CoordMode, ToolTip, Window
CoordMode, Mouse, Window
CoordMode, Pixel, Window

#Include, scrolling.ahk
#Include, bluebox.ahk
#Include, skills.ahk
#include, leveling.ahk


AttackEnemy() {
	global
	ControlClick, x%MainX% y%MainY%, %GameName%,,,, NA
	return
}

ToggleFarm() {
	global
	ControlSend,, {a down}{a up}, %GameName%
	return
}

SetTimer ClickBlueBoxes, 5000
SetTimer AttackEnemy, 80
SetTimer ToggleFarm, 10000

LevelHeroLoop()
SetTimer LevelHeroLoop, 60000

;; 10M skills
sleep 1000
SkillTwoHandQuicken()
SetTimer SkillTwoHandQuicken, 604000
sleep 1000
SkillFury()
SetTimer SkillFury, 604000

;; 30M skills
sleep 1000
SkillEnchantDeadlyPoison()
SetTimer SkillEnchantDeadlyPoison, 1812000
sleep 1000
SkillHindsight()
SetTimer SkillHindsight, 1812000
sleep 1000
SkillMug()
SetTimer SkillMug, 1812000
sleep 1000
SkillMysticalAmplification()
SetTimer SkillMysticalAmplification, 1812000
sleep 1000
SkillBowlingBash()
SetTimer SkillBowlingBash, 1812000
sleep 1000
SkillLexAeterna()
SetTimer SkillLexAeterna, 1812000

;; 8H skills
sleep 1000
SkillMammonite()
SetTimer SkillMammonite, 28992000

SetTimer RecoverGame, 604000

RecoverGame() {
	global
	sleep 1000
	return
}

; Reload Script
^f6::
	Run "%A_AhkPath%" /restart "%A_ScriptFullPath%" %params%
	return

; Pause
f10::Pause
	Suspend
	Pause,,1
	return