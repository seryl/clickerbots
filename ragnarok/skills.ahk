﻿;; Powers
SkillTwoHandQuicken() {
	global
	ControlSend,, 1, %GameName%
	return
}

SkillFury() {
	global
	ControlSend,, 2, %GameName%
	return
}

SkillEnchantDeadlyPoison() {
	global
	ControlSend,, 3, %GameName%
	return
}

SkillHindsight() {
	global
	ControlSend,, 4, %GameName%
	return
}

SkillMug() {
	global
	ControlSend,, 5, %GameName%
	return
}

SkillMammonite() {
	global
	ControlSend,, 6, %GameName%
	return
}

SkillMysticalAmplification() {
	global
	ControlSend,, 7, %GameName%
	return
}

SkillBowlingBash() {
	global
	ControlSend,, 8, %GameName%
	return
}

SkillLexAeterna() {
	global
	ControlSend,, 9, %GameName%
	return
}