﻿ScrollToTop() {
	global
	Loop 3
	{
		ControlClick, x%ScrollX% y%ScrollTopY%, %GameName%,,,, NA
	}
	return
}

ScrollToBot() {
	global
	;ControlClick, x%ScrollX% y%ScrollBotY%, %GameName%,,,, NA
	Loop 80
	{
		ControlClick, x%ScrollX% y%ScrollDownY%, %GameName%,, WheelDown,, NA
	}
	return
}

ScrollUp() {
	global
	ControlClick, x%ScrollX% y%ScrollUpY%, %GameName%,,,, NA
	return
}

ScrollDown() {
	global
	;ControlClick, x%ScrollX% y%ScrollDownY%, %GameName%,,,, NA
	ControlClick, x%ScrollX% y%ScrollDownY%, %GameName%,, WheelDown,, NA
	return
}