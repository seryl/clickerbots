﻿;; Blue Boxes
BBLoc1X = 1000
BBLoc1Y = 474

BBLoc2X = 530
BBLoc2Y = 500

BBLoc3X = 760
BBLoc3Y = 400

BBLoc4X = 1050
BBLoc4Y = 460

BBLoc5X = 748
BBLoc5Y = 455

BBLoc6X = 875
BBLoc6Y = 530

BBLoc7X = 760
BBLoc7Y = 380

BBLoc8X = 746
BBLoc8Y = 437

BBLoc9X = 1004
BBLoc9Y = 452

ClickBlueBoxes() {
	global
	Loop 2
	{
		ControlClick, x%BBLoc1X% y%BBLoc1Y%, %GameName%,,,, NA
		ControlClick, x%BBLoc2X% y%BBLoc2Y%, %GameName%,,,, NA
		ControlClick, x%BBLoc3X% y%BBLoc3Y%, %GameName%,,,, NA
		ControlClick, x%BBLoc4X% y%BBLoc4Y%, %GameName%,,,, NA
		ControlClick, x%BBLoc5X% y%BBLoc5Y%, %GameName%,,,, NA
		ControlClick, x%BBLoc6X% y%BBLoc6Y%, %GameName%,,,, NA
		ControlClick, x%BBLoc7X% y%BBLoc7Y%, %GameName%,,,, NA
		ControlClick, x%BBLoc8X% y%BBLoc8Y%, %GameName%,,,, NA
		ControlClick, x%BBLoc9X% y%BBLoc9Y%, %GameName%,,,, NA
	}
	return
}

SearchBlueBox() {
	global
	ImageSearch, BBLocationX, BBLocationY, 0, 0, A_ScreenWidth, A_ScreenHeight, *80 %A_ScriptDir%\blue_box.png
	if ErrorLevel != 2
		ControlClick, x%BBLocationX% y%BBLocationY%, %GameName%,,,, NA
	return
}