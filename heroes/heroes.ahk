#InstallKeybdHook
#UseHook
#Persistent
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#SingleInstance force
SetControlDelay -1

;; Main
GameName = Clicker Heroes
Paused = false
MainX = 850
MainY = 380

;; Scrolling
ScrollX = 558
ScrollTopY = 260
ScrollBotY = 630
ScrollUpY = 219
ScrollDownY = 653

;; Character Updates
TopChar1X = 110
TopChar1Y = 285

TopChar2X = 110
TopChar2Y = 390

TopChar3X = 110
TopChar3Y = 495

TopChar4X = 110
TopChar4Y = 600

BotChar1X = 100
BotChar1Y = 305

BotChar2X = 100
BotChar2Y = 412

BotChar3X = 100
BotChar3Y = 523

AllSkillsX = 370
AllSkillsY = 585

Skill1X = 200
Skill1Y = 315

ExitX = 906
ExitY = 162

;Skill1X = 200
;Skill1Y = 315

;Skill1X = 200
;Skill1Y = 315

WinGet, GameID, ID, WinTitle %GameName%

SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
CoordMode, ToolTip, Window
CoordMode, Mouse, Window
CoordMode, Pixel, Window

#Include, scrolling.ahk
#Include, fish.ahk
#Include, skills.ahk
#include, leveling.ahk

AttackEnemy() {
	global
	ControlClick, x%MainX% y%MainY%, %GameName%,,,, NA
	return
}

ToggleFarm() {
	global
	ControlSend,, {a down}{a up}, %GameName%
	return
}

getSkillBonusClickable() {
    ; click in a sequential area to try to catch mobile clickable
    Loop 20
    {
   		ControlClick, % "x" 780 " y" 160, %GameName%,,,, NA
   		ControlClick, % "x" 800 " y" 160, %GameName%,,,, NA
   		ControlClick, % "x" 880 " y" 160, %GameName%,,,, NA
   		ControlClick, % "x" 900 " y" 160, %GameName%,,,, NA
   		ControlClick, % "x" 980 " y" 160, %GameName%,,,, NA
   		ControlClick, % "x" 1000 " y" 160, %GameName%,,,, NA
   	}
    return
}

SetTimer ClickOrangeFishes, 5000
SetTimer AttackEnemy, 80
;SetTimer UpdateCharacters, 2000
SetTimer ToggleFarm, 10000
setTimer getSkillBonusClickable, 100000

LevelHeroLoop()
SetTimer LevelHeroLoop, 60000

;; 10M skills
sleep 1000
SkillClickStorm()
SetTimer SkillClickStorm, 604000
sleep 1000
SkillPowerSurge()
SetTimer SkillPowerSurge, 604000

;; 30M skills
sleep 1000
SkillLuckyStrikes()
SetTimer SkillLuckyStrikes, 1812000
sleep 1000
SkillMetalDetector()
SetTimer SkillMetalDetector, 1812000
sleep 1000
SkillGoldenClicks()
SetTimer SkillGoldenClicks, 1812000
sleep 1000
SkillSuperClicks()
SetTimer SkillSuperClicks, 1812000
sleep 1000
SkillEnergize()
SetTimer SkillEnergize, 1812000
sleep 1000
SkillReload()
SetTimer SkillReload, 1812000

;; 8H skills
sleep 1000
SkillDarkRitual()
SetTimer SkillDarkRitual, 28992000

SetTimer RecoverGame, 604000

RecoverGame() {
	global
	sleep 1000
	return
}

; Reload Script
^f5::
	Run "%A_AhkPath%" /restart "%A_ScriptFullPath%" %params%
	return

; Pause
f9::Pause
	Suspend
	Pause,,1
	return