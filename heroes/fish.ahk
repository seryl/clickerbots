﻿;; Orange Fish
OFishLoc1X = 980
OFishLoc1Y = 440

OFishLoc2X = 736
OFishLoc2Y = 412

OFishLoc3X = 860
OFishLoc3Y = 490

OFishLoc4X = 1025
OFishLoc4Y = 425

OFishLoc5X = 525
OFishLoc5Y = 475

OFishLoc6X = 750
OFishLoc6Y = 360

OFishLoc7X = 1050
OFishLoc7Y = 445

OFishLoc8X = 1010
OFishLoc8Y = 450

ClickOrangeFishes() {
	global
	Loop 2
	{
		ControlClick, x%OFishLoc1X% y%OFishLoc1Y%, %GameName%,,,, NA
		ControlClick, x%OFishLoc2X% y%OFishLoc2Y%, %GameName%,,,, NA
		ControlClick, x%OFishLoc3X% y%OFishLoc3Y%, %GameName%,,,, NA
		ControlClick, x%OFishLoc4X% y%OFishLoc4Y%, %GameName%,,,, NA
		ControlClick, x%OFishLoc5X% y%OFishLoc5Y%, %GameName%,,,, NA
		ControlClick, x%OFishLoc6X% y%OFishLoc6Y%, %GameName%,,,, NA
		ControlClick, x%OFishLoc7X% y%OFishLoc7Y%, %GameName%,,,, NA
		ControlClick, x%OFishLoc8X% y%OFishLoc8Y%, %GameName%,,,, NA
	}
	return
}

SearchOrangeFish() {
	global
	ImageSearch, OFishX, OFishY, 0, 0, A_ScreenWidth, A_ScreenHeight, *100 %A_ScriptDir%\fishface.png
	if ErrorLevel != 2
		ControlClick, x%OFishX% y%OFishY%, %GameName%,,,, NA
	return
}