﻿;; Powers
SkillClickStorm() {
	global
	ControlSend,, 1, %GameName%
	return
}

SkillPowerSurge() {
	global
	ControlSend,, 2, %GameName%
	return
}

SkillLuckyStrikes() {
	global
	ControlSend,, 3, %GameName%
	return
}

SkillMetalDetector() {
	global
	ControlSend,, 4, %GameName%
	return
}

SkillGoldenClicks() {
	global
	ControlSend,, 5, %GameName%
	return
}

SkillDarkRitual() {
	global
	ControlSend,, 6, %GameName%
	return
}

SkillSuperClicks() {
	global
	ControlSend,, 7, %GameName%
	return
}

SkillEnergize() {
	global
	ControlSend,, 8, %GameName%
	return
}

SkillReload() {
	global
	ControlSend,, 9, %GameName%
	return
}